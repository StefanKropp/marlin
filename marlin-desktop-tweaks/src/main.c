/*
 * vim: expandtab:ts=2:sts=2:sw=2
 *
 * Copyright (C) 2023 Stefan Kropp <stefan.kropp@posteo.de>
 *
 * This file is part of marlin-desktop-tweaks.
 *
 * marlin-desktop-tweaks is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * marlin-desktop-tweaks is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with marlin-desktop-tweaks. If not, see <http://www.gnu.org/licenses/>.
 */

#define TEXT_WELCOME                                                           \
  "<span foreground=\"blue\" size=\"x-large\">Willkommen bei Debian GNU/Linux</span>\n\n\
<i>Dies ist die Tweaks App für Debian Desktop Marlin</i>\n\n\
https://www.debian.org\
"

#define TEXT_UPDATE \
  "<span foreground=\"blue\" size=\"x-large\">Betriebssystem aktualisieren</span>\n\n\
Für das Betriebssystem werden regelmäßig Fehlerkorrekturen und Sicherheitsaktualisierungen geliefert.\
Es wird empfohlen das Betriebssystem aktuell zu halten. \
\n\n\
<span foreground=\"white\" background=\"black\">apt update &amp;&amp; apt upgrade</span>\n\
"
#define TEXT_UPGRADE \
  "<span foreground=\"blue\" size=\"x-large\">Betriebssystem upgrade</span>\n\n\
"
#define TEXT_NETWORK \
  "<span foreground=\"blue\" size=\"x-large\">Netzwerk Einstellungen</span>\n\n\
Konfiguration von Netwerk und WLAN.\n\
"
#define TEXT_SUDO \
  "<span foreground=\"blue\" size=\"x-large\">Sudo konfiguration und root</span>\n\n\
"

#define TEXT_BACKPORTS \
  "<span foreground=\"blue\" size=\"x-large\">Debian Backports</span>\
\n\n\
Was ist Debian Backports?\n\
Es kann ggf. sein, dass in Debian Stable eine Version einer Software verfügbar ist, wobei es eigentlich schon eine neuere Version gibt. \n\
Um die neuere Version nutzen zu können, müsste man bis zum nächsten Release warten. Das ist nicht immer nötig, es kann sein, dass die \
Version im Backports-Bereich bereitgestellt wird.\n\
\n\
Um Backports nutzen zu können, muss Backports explizit dem System bekanntgegeben werden.\
\n\
<span foreground=\"white\" background=\"black\">deb http://deb.debian.org/debian bookworm-backports main</span>\n\
\n\n\
<span foreground=\"white\" background=\"black\">echo \"deb http://deb.debian.org/debian bookworm-backports main\" | tee -a /etc/apt/sources.list.d/backports.list</span>\n\
"

#include <gtk/gtk.h>

static GtkTextView* buildTextView();

static GtkTextBuffer* buildTextBuffer(const char* text) {
  GtkTextBuffer *buffer = gtk_text_buffer_new(NULL);
  GtkTextIter iter;
  gtk_text_buffer_get_iter_at_line_offset(buffer, &iter, 0, 0);
  gtk_text_buffer_insert_markup(buffer, &iter, text, -1);
  return buffer;
}

static GtkTextView* buildTextView(GtkTextBuffer* buffer) {
  GtkTextView *textView = GTK_TEXT_VIEW(gtk_text_view_new_with_buffer(buffer));
  gtk_text_view_set_editable(GTK_TEXT_VIEW(textView), false);
  gtk_text_view_set_cursor_visible(GTK_TEXT_VIEW(textView), false);
  gtk_text_view_set_wrap_mode(GTK_TEXT_VIEW(textView), GTK_WRAP_WORD);
  return textView;
}

static void activate(GtkApplication *app, gpointer user_data) {
  GtkWidget *window;

  window = gtk_application_window_new(app);
  gtk_window_set_title(GTK_WINDOW(window), "Marlin Desktop Tweaks");
  gtk_window_set_default_size(GTK_WINDOW(window), 200, 200);

  GtkTextView *text_welcome = buildTextView(buildTextBuffer(TEXT_WELCOME)); 
  GtkTextView *text_update = buildTextView(buildTextBuffer(TEXT_UPDATE));
  GtkTextView *text_upgrade = buildTextView(buildTextBuffer(TEXT_UPGRADE));
  GtkTextView *text_network = buildTextView(buildTextBuffer(TEXT_NETWORK));
  GtkTextView *text_sudo = buildTextView(buildTextBuffer(TEXT_SUDO));
  GtkTextView *text_backports = buildTextView(buildTextBuffer(TEXT_BACKPORTS));
  GtkWidget *box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);

  GtkWidget *s = gtk_stack_sidebar_new();
  GtkWidget *stack = gtk_stack_new();
  gtk_widget_set_hexpand(stack, true);
  gtk_widget_set_vexpand(stack, true);
  gtk_widget_set_hexpand(box, true);
  gtk_widget_set_vexpand(box, true);
  gtk_stack_set_transition_type(GTK_STACK(stack),
                                GTK_STACK_TRANSITION_TYPE_SLIDE_UP);

  GtkWidget *box1 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_widget_set_hexpand(box1, true);
  gtk_widget_set_vexpand(box1, true);

  gtk_stack_add_titled(GTK_STACK(stack), GTK_WIDGET(text_welcome), "welcome", "Willkommen");
  gtk_stack_add_titled(GTK_STACK(stack), GTK_WIDGET(text_update), "update", "System update");
  gtk_stack_add_titled(GTK_STACK(stack), GTK_WIDGET(text_upgrade), "upgrade", "System upgrade");
  gtk_stack_add_titled(GTK_STACK(stack), GTK_WIDGET(text_network), "network", "Network");
  gtk_stack_add_titled(GTK_STACK(stack), GTK_WIDGET(text_sudo), "sudo", "sudo");
  gtk_stack_add_titled(GTK_STACK(stack), GTK_WIDGET(text_backports), "backports", "Backports");
  gtk_stack_sidebar_set_stack(GTK_STACK_SIDEBAR(s), GTK_STACK(stack));
  gtk_box_append(GTK_BOX(box), s);
  gtk_box_append(GTK_BOX(box), stack);
  gtk_window_set_child(GTK_WINDOW(window), box);
  gtk_widget_show(window);
}

int main(int argc, char **argv) {
  GtkApplication *app;
  int status;

  app = gtk_application_new("org.debian.marlin.marlin-desktop-tweaks",
                            G_APPLICATION_DEFAULT_FLAGS);
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);
  status = g_application_run(G_APPLICATION(app), argc, argv);
  g_object_unref(app);

  return status;
}
