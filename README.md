# Debian Marlin - A Debian Desktop Environment

The goal of Debian Desktop Pure Blend is to build an Debian System for Desktop /
Office / Laptops. A Debian System which "Just works".

The Debian Desktop project will provide an Desktop Environment with Codename
"Marlin" - the clownfish in the movie "Finding Nemo".

# Metapackage - marlin-desktop

The metapackage marlin-desktop provides an xfce-desktop with a set of
applications for daily work. Evolution is a Groupware-Suite with mail client,
calendar, address book, to-do list and memo tools. The package will also provide
some multimedia applications like vlc and audacious. LibreOffice is a
full-featured office productivity suite.

# Metapackage - marlin-extra-*

The metapackages marlin-extra-* will provide additional software for special
use cases.

 * marlin-extra-multimedia
 * marlin-extra-internet

# Package - marlin-desktop-config

Configuration for Marlins' desktop environment.
